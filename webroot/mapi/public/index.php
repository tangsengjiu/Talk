<?php

// include Yii bootstrap file
defined('YII_ENV') or define('YII_ENV', 'dev');
defined('YII_DEBUG') or define('YII_DEBUG', true);

require_once(__DIR__ . '/../../lib/vendor/autoload.php');
require_once(__DIR__ . '/../../lib/vendor/yiisoft/yii2/Yii.php');
$config = require_once(__DIR__ . '/../protected/config/main.php');

Yii::setAlias('@source','/assets');
Yii::setAlias('@app','/../protected');

(new yii\web\Application($config))->run();

