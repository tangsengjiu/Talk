<?php
use yii\helpers\Html;
use common\assets\AppAsset;
use yii\widgets\Breadcrumbs;
/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/6/23
 * Time: 18:24
 */
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= Html::encode($this->title) ?></title>
	<?= Html::csrfMetaTags() ?>
	<?php $this->head() ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<?php $this->beginBody() ?>
	<?php
	if(!yii::$app->user->isGuest)
	{
	?>
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="#" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>B</b>M</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Bao</b>Map</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
<!--					<ul class="nav navbar-nav">-->
<!---->
<!--					</ul>-->
					<ul class="nav navbar-top-links navbar-right">
						<li>
<!--							<a href="/user/logout">-->
<!--								<i class="fa fa-sign-out"></i> Log out-->
<!--							</a>-->
						</li>
						<li>
							<a class="right-sidebar-toggle">
								<i class="fa fa-tasks"></i>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">

				<div class="user-panel">
					<div class="pull-left image">
						<?= Html::img( AppAsset::getImgPath().'xunbao2.jpg',
							['class'=>"img-circle",'alt'=>"User Image"]) ?>
					</div>
					<div class="pull-left info">
						<p><?= Yii::$app->user->identity->username ?></p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>

				<?php
					function activeTreeview($items)
					{
						if(is_array($items))
						{
							$controller = Yii::$app->controller->id;
							//$action = Yii::$app->controller->action->id;
							if(in_array($controller, $items))
							{
								return 'active';
							}
						}
						return '';
					}
				?>

				<ul class="sidebar-menu">
					<li class="header">导航</li>

					<li class=" treeview <?= activeTreeview(['activity','activity-category','goods-attribute','goods','supplier'])?>">
						<a href="#">
							<i class="fa fa-dashboard"></i> <span>产品管理</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">

							<li>
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'母产品列表', ['pproduct/index']) ?>
							</li>
							<li>
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'添加母产品', ['pproduct/create']) ?>
							</li>


							<li>
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'子产品', ['activity/index']) ?>
							</li>

							<li>
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'净值列表', ['product/net-page']) ?>
							</li>

							<li>
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'添加净值', ['product/add-net']) ?>
							</li>

						</ul>
					</li>

					<li class=" treeview <?= activeTreeview(['order'])?>">
						<a href="#">
							<i class="fa fa-files-o"></i>
							<span>用户管理</span>
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">

							<li >
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'添加用户', ['user/add']) ?>
							</li>

							<li >
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'所有用户', ['user/manage']) ?>
							</li>

						</ul>
					</li>

					<li class=" treeview <?= activeTreeview(['channel'])?>">
						<a href="#">
							<i class="fa fa-files-o"></i>
							<span>渠道管理</span>
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">

							<li >
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'渠道列表', ['channel/index']) ?>
							</li>


						</ul>
					</li>

					<li class=" treeview <?= activeTreeview(['news','news-category'])?>">
						<a href="#">
							<i class="fa fa-files-o"></i>
							<span>资讯管理</span>
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">

							<li >
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'资讯列表', ['news/index']) ?>
							</li>

							<li >
								<?= Html::a('<i class="fa fa-circle-o"></i> '.'资讯栏目', ['news-category/index']) ?>
							</li>

						</ul>
					</li>

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					<?= Html::encode($this->title) ?>
				</h1>
				<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				]) ?>
			</section>

			<section class="content">
				<br>
				<?= $content ?>
			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.3.0
			</div>
			<strong>寻宝图后台系统 v0.1 <a href="http://baomap.com">baomap</a>.</strong> All rights reserved.
		</footer>

	</div> <!-- ./wrapper -->
	<?php }else{ ?>
			<?=$content?>
	<?php }?>
	<?php $this->endBody() ?>
</body>
</html>
<!---->
<?php $this->endPage() ?>
