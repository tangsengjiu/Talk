<?php
/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/7/14
 * Time: 14:37
 */

namespace app\controllers;


use yii\rest\ActiveController;
use common\models\Pproduct;
class ProductController extends ActiveController
{
	public $modelClass = "common\models\Pproduct";
	public function actions()
	{
		return [];
	}

	public function actionOne($ppid)
	{
		$pproduct = Pproduct::findAll(['ppid'=>$ppid]);
		return ['pproduct'=>$pproduct];
	}

	public function actionPproductlist()
	{
		$pproducts = Pproduct::find()->all();
		return ['pproducts'=>$pproducts];
	}
}