<?php
namespace app\controllers;
use app\models;
use Yii;
use yii\rest\ActiveController;
use common\models\News;

/**
 * SiteController is the default controller to handle user requests.
 */
class NewsController extends ActiveController
{
	public $modelClass = 'app\models\News';

	public function actions()
	{
		return [];
	}
	
	public function actionIndex($id)
	{
		$news = News::findAll(['id'=>$id]);
		return ['news'=>$news];
	}
}