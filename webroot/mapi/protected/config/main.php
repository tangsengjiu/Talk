<?php

$commonConfig = require_once(__DIR__ . '/../../../lib/common/config/main.php');
$baseConfig = require_once('base.php');

$config = array_merge($commonConfig, $baseConfig);
$config = array_merge($config);
return $config;
