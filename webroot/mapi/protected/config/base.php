<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/3
 * Time: 17:11
 */
defined('STATIC_VER') or define('STATIC_VER', '20160507001');
$basePath = "../protected";
Yii::setAlias('BaseController',$basePath . '/components/BaseController.php');
Yii::$classMap['BaseController'] = $basePath . '/components/BaseController.php';

return array(
	'id' => 'basic',
	'name' => 'Basic',
	'basePath' => "../protected",
	'defaultRoute' => 'news/index',
	'params' => array(
		'accessRules' =>array(
			array('deny',  // 所有用户有操作权限：index,view
					'actions'=>array('user'),
					'controllers' => array('index', 'login'),
					'users'=>array('*'),
			),
			array('deny', // 仅登录用户有权限操作：create,update
					'actions'=>array('user'),
					'controllers' => array('adduser', 'manage', 'level'),
					'users'=>array('?'),
					'deniedCallback' => array('LUtil', 'redirectIndex'),
			),
		),
	)
);