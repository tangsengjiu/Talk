requirejs.config({
	baseUrl: '/assets/src/js/',
	shim: {
		'jquery-form': [
			'jquery'
		],
		'jquery.validation': [
			'jquery'
		],
		'jquery.cookie': [
			'jquery'
		],
		'jquery-migrate': [
			'jquery'
		],
		'jquery-hashchange': [
			'jquery',
			'jquery-migrate'
		],
		'highcharts': {
            //deps: ['jquery']
			//deps: ['highcharts-adapter'],
			//exports: 'Highcharts'
		},
		'handlebars': {
			exports: 'handlebars'
		},
		'jscrollpane': {
			deps: ['jquery']
		},
		'mousewheel': {
			deps: ['jquery']
		},
        'jquery-ui': {
            deps: ['jquery']
        }
	},
	paths: {
		'jquery': '../../bower_components/jquery/dist/jquery-2.2.3.min',
		'underscore': '../../bower_components/underscore/underscore',
		'jquery.validation': '../../bower_components/jquery.validation/dist/jquery.validate',
		'jquery.form': '../../bower_components/jquery-form/jquery.form',
        'jquery-hashchange': '../../bower_components/jquery-hashchange/jquery.ba-hashchange.min',
		'highcharts': '../../bower_components/highcharts/highcharts',
        'handlebars': '../../bower_components/handlebars/handlebars.min',
        'text': '../../bower_components/requirejs-text/text'
	}
});