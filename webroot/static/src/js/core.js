require([
	'jquery',
	'underscore',
	'jquery-hashchange',
	'jquery.validation',
	'jquery.form',
	'highcharts'
],function($, _){
	$.global = window.global || {};
	$.global.timeDelta = $.global.timeDelta || 0;

	$(".J_login_action").on('click',function(){
		location.href = $(this).data("url");
	});

	$(".J_login_out").on('click',function(){
		location.href = $(this).data("url");
	});
	$.getTime = function(time) {
		time = time || $.now();
		return parseInt(time / 1000, 10) + $.global.timeDelta;
	};


	var origInit = $.validator.prototype.init;
	$.validator.prototype.init = function() {
		origInit.apply(this, arguments);
		$(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, " +
				"[type='number'], [type='search'] ,[type='tel'], [type='url'], " +
				"[type='email'], [type='datetime'], [type='date'], [type='month'], " +
				"[type='week'], [type='time'], [type='datetime-local'], " +
				"[type='range'], [type='color'] ",
				"keydown", function(event) {
					var timeout = this.data('keyup-timeout');
					if (timeout) {
						clearTimeout(timeout);
						this.data('keyup-timeout', null);
					}
				});
	};

	$.validator.prototype.element = function(element) {
		var cleanElement = this.clean(element),
				checkElement = this.validationTargetFor(cleanElement),
				result = true;

		this.lastElement = checkElement;

		if (checkElement === undefined) {
			delete this.invalid[cleanElement.name];
		} else {
			this.prepareElement(checkElement);
			this.currentElements = $(checkElement);

			result = this.check(checkElement);
			if (result) {
				delete this.invalid[checkElement.name];
			} else {
				this.invalid[checkElement.name] = true;
			}
		}
		// Add aria-invalid status for screen readers
		if (result === false || result === true) {
			$(element).attr("aria-invalid", !result);
		} else {
			$(element).attr("aria-invalid", 'pending');
		}

		if (!this.numberOfInvalids()) {
			// Hide error containers on last error
			this.toHide = this.toHide.add(this.containers);
		}
		this.showErrors();
		return result;
	};

	var origShowLabel = $.validator.prototype.showLabel;
	$.validator.prototype.showLabel = function(element, message) {
		if (message && this.settings.messageWrapper) {	// 支持使用简单标签wrap错误信息，比如使用p,div,span等
			message = '<' + this.settings.messageWrapper + '>' + message + '</' + this.settings.messageWrapper + '>';
		}

		if (message && this.settings.messageDecoration) {
			message += this.settings.messageDecoration;
		}
		origShowLabel.call(this, element, message);
	};

	$.validator.prototype.errors = function() {
		var errorClass = this.settings.errorClass.split(" ").join(".");
		var successClass = this.settings.validClass.split(" ").join(".");
		return $(this.settings.errorElement + "." + errorClass, this.errorContext).add($(this.settings.errorElement + "." + successClass, this.errorContext));
	};

	var origRemote = $.validator.methods.remote;
	$.validator.methods.remote = function(value, element, param) {
		var previous = this.previousValue(element), $element = $(element), validator = this, paramObj;
		if (typeof param === 'string') {
			if (param.indexOf('{') != -1) {
				try {
					paramObj = $.parseJSON(param);
				} catch (e) {
					paramObj = {url: param};
				}
			} else {
				paramObj = {url: param};
			}
		} else {
			paramObj = param;
		}
		paramObj = $.extend(true, {
			type: $element.data('remote-type') || 'POST',
			dataType: $element.data('remote-datatype') || 'json',
			success: function(response) {
				if (response.retCode == 100002) {
					Dialog.alert('', '登录超时，请重新登录。').done(function() {
						location.href = '/user/login?returnUrl=' + encodeURIComponent(location.href);
					});
				}
				var valid = response.retCode === 0, errors, message, submitted;
				errors = {};
				validator.settings.messages[element.name].remote = previous.originalMessage;
				if (valid) {
					$element.attr('aria-invalid', 'false');
					submitted = validator.formSubmitted;
					validator.prepareElement(element);
					validator.formSubmitted = submitted;
					validator.successList.push(element);
					delete validator.invalid[element.name];
					if (response.retData.callback && $.isFunction(validator.settings[response.retData.callback])) {
						validator.settings[response.retData.callback].call(validator, response);
					}
				} else {
					$element.attr('aria-invalid', 'true');
					message = response.retMsg || validator.defaultMessage(element, "remote");
					errors[element.name] = previous.message = $.isFunction(message) ? message(value) : message;
					validator.invalid[element.name] = true;

				}
				previous.valid = valid;
				validator.stopRequest(element, valid);
				validator.showErrors(errors);
			}
		}, paramObj);
		return origRemote.call(this, value, element, paramObj);
	};

	$.validator.setDefaults({
		keyupDelay: 500, // 默认keyup事件延时500ms
		messageWrapper: 'p',
		messageDecoration: '',
        errorClass: "unchecked",
        validClass: "checked",
		//errorClass: 'notice_bar notice_error',
		//validClass: 'notice_bar notice_success',
		errorElement: 'div',
		ignore: ':hidden,.ignore',
		errorPlacement: function(error, element) {
			var s = element.parent().find("span[htmlFor='" + element.attr("id") + "']");
			if(s!=null){
				s.remove();
			}
			error.appendTo(element.parent());
		},
		onkeyup: function(element, event) {
			if (event.which !== 9 && $(element).attr('aria-invalid') == 'true'/*this.numberOfInvalids()*/) {
				var _THIS = this;
				var timeout = $(element).data('keyup-timeout');
				if (timeout) {
					clearTimeout(timeout);
				}
				timeout = setTimeout(function() {
					$(element).data('keyup-timeout', null);
					_THIS.element(element);
				}, _THIS.settings.keyupDelay);
				$(element).data('keyup-timeout', timeout);
			}
		},
		showErrors: function(errorMap, errorList) {
			if (this.pendingRequest == 0) {
				var $submitBtn = $(this.currentForm).find(':submit');
				if (!this.numberOfInvalids()) {
					$submitBtn.removeClass('btn_disabled');
				} else {
					$submitBtn.addClass('btn_disabled');
				}
				this.defaultShowErrors();
			}
		},
		success: function($label, element) {
			if (!$(element).data('ignore-success')) {
				$label.removeClass(this.errorClass).addClass(this.validClass).html('&nbsp;');
			} else {
				$label.remove();
			}
		}
	});
	//$.validator.methods.email = function(value, element) {
	//	return this.optional(element) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/.test(value);
	//};
	$.validator.addMethod('email', function(value, element){
		return this.optional(element) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/.test(value);
	}, '邮箱格式不正确');
	$.validator.addMethod('mobile', function(value, element) {
		return this.optional(element) || /^1\d{10}$/.test(value);
	}, '手机号格式不正确');

	$.validator.addMethod('pwdStrength', function(value, element, param) {
		return this.optional(element) || (/^\S{6,20}$/.test(value) && strenth(value) >= Number(param));
	}, '密码长度为6-20数字、字符、英文字母');

	$.validator.addMethod('idcard', function(value, element, param) {
		return this.optional(element) || idcard(value.toUpperCase());
	}, '身份证号码无效');

	$.validator.addMethod('age', function(value, element, param) {
		if (this.optional(element) || !idcard(value)) {
			return true;
		}
		var birthday = parseInt(value.substring(6, 14), 10);
		return birthday <= parseInt(param, 10);
	}, '根据相关规定，18岁以下不允许进行开户');
	return $;
});