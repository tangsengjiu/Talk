/**
 * Created by wukong on 2016/9/26.
 */
define([
    'core'
],function($){
    return {
        validateForm: function(args){
            var inputName = args.inputName;
            captcha = args.captcha;
            args.form.validate({
                ignore:[],
                focusCleanup:true,
                focusInvalid:false,
                errorClass: "unchecked",
                validClass: "checked",
                messageDecoration: '',
                errorElement: "span",
                submitHandler: function(form) {
                    var validator = this;
                    if (!validator.submitting) {
                        validator.submitting = true;
                        $(form).find(':submit').attr('disabled', 'disabled').addClass('btn_disabled');
                        $(form).ajaxSubmit({
                            dataType: "json",
                            success: function(ret) {
                                var errorTip = {};
                                errorTip[inputName] = ret.retMsg;
                                if (ret.retCode == 0) {
                                    location.href = ret.retData.returnUrl || '/';
                                } else {
                                    if(args.needCaptcha){
                                        captcha.refresh();
                                    }
                                    validator.showErrors(errorTip);
                                }
                            },
                            error: function() {
                                if(args.needCaptcha){
                                    captcha.refresh();
                                }
                                var errorTip = {};
                                errorTip[inputName] = '网络有一点问题，请刷新后重试';
                                validator.showErrors(errorTip);
                            },
                            complete: function() {
                                validator.submitting = false;
                                $(form).find(':submit').removeClass('btn_disabled').removeAttr('disabled');
                            }
                        });
                    }
                },
                errorPlacement:function(error,element){
                    var s = element.parent().find("span[htmlFor='" + element.attr("id") + "']");
                    if(s!=null){
                        s.remove();
                    }
                    error.appendTo(element.parent());
                },
                success: function(label) {
                    label.removeClass("unchecked").addClass("checked");
                },
                showErrors: function(errorMap, errorList) {
                    if (this.pendingRequest == 0) {
                        var $submitBtn = $(this.currentForm).find(':submit');
                        if (!this.numberOfInvalids()) {
                            $submitBtn.removeClass('btn_disabled');
                        } else {
                            $submitBtn.addClass('btn_disabled');
                        }
                        this.defaultShowErrors();
                        if(args.isMobile){
                            var $unCheck = $(".unchecked");
                            if($unCheck.size() > 0){
                                var msg = $unCheck.eq(0).find('p').html();
                                if(msg != ""){
                                    $.toast({
                                        text: msg,
                                        loader: false,
                                        hideAfter: 1000,
                                        stack:1
                                    });
                                }
                            }
                        }
                    }
                },
                highlight: false,
                unhighlight: false
            });
        }
    }
});
