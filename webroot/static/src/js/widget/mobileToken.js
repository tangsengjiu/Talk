define([
	'core',
	'widget/counter'
], function($, Counter) {
	var defaults = {
		form: 'form',
		btn: 'input[name=tokenBtn]',
		tokenInput: 'input[name=token]',
		mobileInput: 'input[name=mobile]',
		disableClass: 'btn_disabled',
		btnText: '获取验证码',
		btnDoneText: '已发送 ',
		type: '',
		voiceContainer: '.J_voice'
	};

	var MobileToken = function(options) {
		if (typeof options === 'string') {
			options = {form: options};
		}
		this.options = $.extend(true, {}, defaults, options);
		this.isInit = false;
		this.sending = false;
		this.init();
	};

	$.extend(MobileToken.prototype, {
		init: function() {
			var _THIS = this;
			if (!this.isInit) {
				this.isInit = true;

				this.$form = $(this.options.form);
				this.$sendBtn = this.$form.find(this.options.btn);
				this.url = this.$sendBtn.data('url');
				this.hash = this.$sendBtn.data('hash');

				this.$voice = $(this.options.voiceContainer);
				this.voiceUrl = this.$voice.data('url');

				this.$tokenInput = this.$form.find(this.options.tokenInput);
				this.$tokenInput.data('widget', this);
				this.$mobileInput = this.$form.find(this.options.mobileInput);
				this.counter = Counter.getCounter(0, function(remain) {
					_THIS.$sendBtn.addClass(_THIS.options.disableClass).val(_THIS.options.btnDoneText + '(' + remain + ')');
				}, function() {
					_THIS.$sendBtn.removeClass(_THIS.options.disableClass).val(_THIS.options.btnText);
					_THIS.$voice.hide();
				});
				this.$sendBtn.on('click', function(e) {
					e.preventDefault();
					_THIS.send();
				});
				this.$voice.find('.J_voice_btn a').on('click', function(e) {
					e.preventDefault();
					_THIS.sendVoice();
				});

				$.validator.addMethod('token', function(value, element) {
					if (this.optional(element)) {
						return true;
					}

					var _THIS = $(element).data('widget');
					if (!_THIS.hash) {
						return false;
					}
					for (var i = value.length - 1, h = 0; i >= 0; --i) {
						h += value.charCodeAt(i);
					}
					return h == _THIS.hash;
				}, '验证码不正确');
			}
		},

		send: function() {
			var _THIS = this, mobile = '';
			if (this.$mobileInput.length) {
				if (this.$mobileInput.val() && !this.$mobileInput.attr('aria-invalid')) {
					var validator = this.$mobileInput.parents('form').data('validator');
					if (validator) {
						validator.element(this.$mobileInput);
					}
				}
				if (this.$mobileInput.attr('aria-invalid') === 'pending') {
					if (!this.timeout) {
						this.timeout = setTimeout(function() {
							_THIS.timeout = null;
							_THIS.send();
						}, 200);
					}
					return;
				}
				if (!this.timeout && this.$mobileInput.val() && (!this.$mobileInput.attr('aria-invalid') || this.$mobileInput.attr('aria-invalid') === 'false')) {
					mobile = this.$mobileInput.val();
				} else {
					return;
				}
			}
			if (this.url && !this.sending && !this.counter.isCounting()) {
				this.sending = true;
				this.$tokenInput.val('');
				$.ajax({
					url: this.url,
					data: {
						mobile: mobile,
						type: this.options.type
					},
					dataType: "json"
				}).done(function(ret) {
					if (ret.retCode == 0) {
						_THIS.hash = ret.retData.hash;
						_THIS.counter.period = ret.retData.resendDelay;
						_THIS.counter.start();
						_THIS.voiceToken = ret.retData.voiceToken;
						_THIS.voiceMobile = ret.retData.voiceMobile;
						_THIS.$voice.find('.J_voice_btn').show();
						_THIS.$voice.find('.J_voice_result').hide();
						_THIS.$voice.show();
					} else if(ret.retCode == "100003"){
						$.global.needValidate = false;
						error = "<div id='" +_THIS.$mobileInput.attr("name") +"-error' class='notice_bar notice_error'><p>"+ ret.retMsg+"</p></div>";
						_THIS.$mobileInput.parent().append(error);
						_THIS.$mobileInput.parent().find("#" +_THIS.$mobileInput.attr("name") +"-error").show(1000).hide();
					}
				}).always(function() {
					_THIS.sending = false;
				});
			}
		},

		sendVoice: function() {
			var _THIS = this;
			if (this.voiceToken && this.voiceMobile && !this.sendingVoice) {
				this.sendingVoice = true;
				$.ajax({
					url: this.voiceUrl,
					data: {
						mobile: this.voiceMobile,
						token: this.voiceToken
					}
				}).done(function(ret) {
					if (ret.retCode == 0) {
						_THIS.voiceToken = _THIS.voiceMobile = '';
						_THIS.$voice.find('.J_voice_btn').hide();
						_THIS.$voice.find('.J_voice_result').show();
					} else {
						$.Dialog.alert('发送失败', '发生了一点小问题，请稍后刷新重试～').done(function() {
							location.reload();
						});
					}
				}).always(function() {
					_THIS.sendingVoice = false;
				});
			}
		}
	});

	return MobileToken;
});