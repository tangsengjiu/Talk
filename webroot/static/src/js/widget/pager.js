/**
 * Created by wukong on 2016/9/9.
 */
define(['core'], function($) {
    return {
        //$bar, page, lastPage, numLimit, clickFunction
        showPageBar: function (args)
        {
            args.$bar.html(this.getPageData(args.paramData.page, args.lastPage, args.numLimit));
            $('li:not(.disabled)', args.$bar).click(function(event)
            {
                event.preventDefault();
                var pageToChange;
                if($('a', this).text()== '<'){
                    pageToChange = parseInt($(this).closest('ul').find("li .new-page-active").text()) - 1;
                }else if($('a', this).text()== '>'){
                    pageToChange = parseInt($(this).closest('ul').find("li .new-page-active").text()) + 1; // 点击下一页图标按钮
                }else{
                    pageToChange = parseInt($('a', this).text());// 点击数字按钮
                }
                pageToChange = (pageToChange >  args.lastPage) ? args.lastPage : pageToChange;
                var params = args.paramData; // 获取分页数据源参数（兼容多个请求参数，page为通用的参数，其余的参数原值传过去）
                params.page = pageToChange;
                args.clickFunction(params);
            });
        },

        //获取分页的页面html数据
        getPageData: function(page, lastPage, numLimit )
        {
            var prePage = '', nextPage = '', currentLeft = '', current = '', currentRight = '';

            if(lastPage == 0 || (page == 1 && lastPage == 1 ))
                return '';

            if(page != lastPage) {
                nextPage = '<li><a href="#">&gt;</a></li>';
            }else{
                nextPage = '<li class="disabled"><a class="invalid-page">&gt;</a></li>';//最后一页不可点击下一页图标
            }

            if(page != 1) {
                prePage  = '<li><a href="#">&lt;</a></li>';
            }else{
                prePage = '<li  class="disabled"><a class="invalid-page">&lt;</a></li>';//第一页时不可点击下一页图标
            }

            current +=  '<li><a  class="new-page-active" href="#">'+page+'</a></li>';

            for(var i = page+1; i<= page+numLimit; i++) {
                if(i > lastPage)
                    break;
                if(i == page+numLimit && i < lastPage) {
                    currentRight += '<li class="disabled"><a  class="invalid-page">...</a></li>';
                    currentRight += '<li><a href="#">'+lastPage+'</a></li>';
                }else {
                    currentRight += '<li><a href="#">'+i+'</a></li>';
                }

            }

            for(var i=page-numLimit; i<page ; i++) {
                if(i<1)
                    continue;
                if(i == page-numLimit && i >1) {
                    currentLeft += '<li><a href="#">1</a></li>';
                    currentLeft += '<li class="disabled"><a class="invalid-page">...</a></li>';
                }else {
                    currentLeft += '<li><a href="#">'+i+'</a></li>';
                }
            }

            var str = '<ul>'+prePage+currentLeft+current+currentRight+nextPage+'</ul>';
            return str;
        }
    }
});
