define([
	'core'
], function($) {

	var defaults = {
		container: '#captcha_container',
		img: '.J_captcha',
		input: 'input[name=captcha]',
		ignoreClass: 'ignore',
		caseSensitive: false
	};

	var Captcha = function(options) {
		if (typeof options === 'string') {
			options = {container: options};
		}
		this.options = $.extend(true, {}, defaults, options);
		this.isInit = false;
		this.refreshing = false;
		this.init();
	};

	$.extend(Captcha.prototype, {
		init: function() {
			var _THIS = this;

			if (!this.isInit) {
				this.isInit = true;
				this.$container = $(this.options.container);
				this.url = this.$container.data('url');
				this.hash = this.$container.data('hash');

				this.$img = this.$container.find(this.options.img);
				this.$input = this.$container.find(this.options.input);

				this.$img.on('click', function(e) {
					e.preventDefault();
					_THIS.refresh();
				});

				$.validator.addMethod('captcha', function(value, element) {
					if (this.optional(element)) {
						return true;
					}

					var valid = false;
					if (_THIS.hash) {
						value = _THIS.options.caseSensitive ? value : value.toLowerCase();
						for (var i = value.length - 1, h = 0; i >= 0; --i) {
							h += value.charCodeAt(i);
						}
						if (h == _THIS.hash) {
							valid = true;
						}
					}

					return valid;
				}, '验证码不正确');
			}
		},
		show: function() {
			this.$input.removeClass(this.options.ignoreClass);
			this.$container.show();
			return this;
		},
		refresh: function() {
			var _THIS = this;
			if (this.url && !this.refreshing) {
				this.refreshing = true;
				console.log(this.url);
				$.ajax({
					url: this.url,
					dataType: 'json',
					cache: false
				}).done(function(data) {
					_THIS.$img.attr('src', data['url']);
					_THIS.hash = _THIS.options.caseSensitive ? data['hash1'] : data['hash2'];
				}).always(function() {
					_THIS.refreshing = false;
					_THIS.$input.val('');
				});
			}
			return this;
		},
		hide: function() {
			this.$container.hide();
			this.$input.addClass(this.options.ignoreClass);
			return this;
		}
	});

	return Captcha;
});