define(function() {
	/**
	 * 判断某个字符是属于哪一类(根据返回的ASCII码来判断)
	 * @param char
	 * @returns {number}
	 */
	function charMode(char) {
		if (char >= 48 && char <= 57) { //数字
			return 1;
		}
		if (char >= 65 && char <= 90) { //大写字母
			return 2;
		}
		if (char >= 97 && char <= 122) { //小写
			return 4;
		}
		return 8; //特殊字符
	}
	/**
	 * 计算出当前密码当中一共有多少种模式
	 * @param n
	 * @returns {number}
	 */
	function bitTotal(n) {
		var m = 0;
		for (var i = 0; i < 4; i++) {
			if (n & 1) {
				m++;
			}
			n >>>= 1;
		}
		return m;
	}

	/**
	 * 返回密码的强度级别
	 * @param c
	 * @returns {number}
	 */
	function checkStrong(c) {
		if (c.length <= 5) { //密码太短
			return 0;
		}
		var m = 0;
		for (var i = 0; i < c.length; i++) {
			//测试每一个字符的类别并统计一共有多少种模式
			m |= charMode(c.charCodeAt(i));
		}
		return bitTotal(m);
	}

	return function(password) {
		return checkStrong(password);
	}
});