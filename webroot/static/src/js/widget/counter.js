define([
	'core'
], function($) {
	var Counter = function(period, counterCallback, readyCallback) {
		this.period = period || 0;
		this.counterCallback = $.isFunction(counterCallback) ? counterCallback : $.noop;
		this.readyCallback = $.isFunction(readyCallback) ? readyCallback : $.noop;
		this.endTime = 0;
		this.interval = null;
		this.started = false;
	};

	Counter.getCounter = function(period, counterCallback, readyCallback) {
		return new Counter(period, counterCallback, readyCallback);
	};

	$.extend(Counter.prototype, {
		start: function() {
			if (!this.started) {
				var _THIS = this;
				this.started = true;
				this.endTime = $.getTime() + this.period;
				this.updateStatus();
				this.interval = setInterval(function() {
					_THIS.updateStatus();
					if (!_THIS.isCounting() && _THIS.interval) {
						clearInterval(_THIS.interval);
						_THIS.interval = null;
						_THIS.started = false;
					}
				}, 1000);
			}

			return this;
		},

		stop: function(stopUpdate) {
			if (this.started && this.interval) {
				clearInterval(this.interval);
				this.interval = null;
				if (!stopUpdate) {
					this.updateStatus();
				}
				this.started = false;
			}

			return this;
		},

		isCounting: function() {
			return this.started && this.endTime && $.getTime() < this.endTime;
		},

		updateStatus: function() {
			if (this.isCounting()) {
				var remain = this.endTime - $.getTime();
				this.counterCallback.call(this, remain);
			} else {
				this.readyCallback.apply(this);
			}
		}
	});

	return Counter;
});