<?php
/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/9/19
 * Time: 16:48
 */
use \common\assets\AppAsset;
use \common\assets\WebAsset;
use \common\assets\HomeAsset;
use yii\web\View;
use \kartik\helpers\Html;
WebAsset::register($this);
WebAsset::addCss($this,WebAsset::getCssPath().'home.css');
AppAsset::addScript($this, \common\assets\WebAsset::getJsPath().'require.js');
AppAsset::addScript($this, \common\assets\WebAsset::getJsPath().'config.js');
WebAsset::addScript($this, \common\assets\WebAsset::getJsPath().'main/index.js');
$imagePath = AppAsset::getImgPath();
$this->title = "专注相声20年";
?>

<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="<?= Yii::$app->language ?>">

<head>
	<title><?= Html::encode($this->title) ?></title>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="专注听相声" />
	<meta name="keywords" content="专注听相声,德云社，相声，岳云鹏，张云雷，郭德纲" />
	<?= Html::csrfMetaTags() ?>
	<?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<div id="header">
	<h1>City Gallery</h1>
</div>

<div id="nav">
	London<br>
	Paris<br>
	Tokyo<br>
</div>
<?=$content?>

<div id="footer">
	Copyright@ 苏ICP备14053842号-1
</div>
<?php $this->endBody() ?>

</body>
</html>

<?php $this->endPage() ?>
