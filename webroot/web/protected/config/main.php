<?php

$commonConfig = require_once(__DIR__ . '/../../../lib/common/config/main.php');
$baseConfig = require_once('base.php');
defined('YII_DEBUG') or define('YII_DEBUG', true);
$config = \yii\helpers\ArrayHelper::merge($commonConfig, $baseConfig);
return $config;
