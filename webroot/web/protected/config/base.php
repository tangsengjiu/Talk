<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/3
 * Time: 17:11
 */
defined('STATIC_VER') or define('STATIC_VER', '20160507001');
$basePath = "../protected";
Yii::setAlias('BaseController',$basePath . '/components/BaseController.php');
Yii::$classMap['BaseController'] = $basePath . '/components/BaseController.php';

return array(
	'id' => 'basic',
	'name' => 'crossTalk',
	'basePath' => "../protected",
	'defaultRoute' => '/home/index',
	'components' =>array(
		'assetManager' => [
			'bundles' => [
			'yii\web\JqueryAsset' => [
			'sourcePath' => null,
				'js' =>[]
				],
			'yii\bootstrap\BootstrapAsset' => [
					'css' => [],
				],
			'yii\bootstrap\BootstrapPluginAsset' => [
					'js' =>[]
				],
			],
		],
		'request' => [
				'enableCookieValidation' => true,
				'enableCsrfValidation' => true,
				'cookieValidationKey' => 'sjjd8jksjw02kdkjsk',
		],
		'user' => [
			'identityClass' => 'common\models\User',
			'enableAutoLogin' => true,
			'loginUrl' => "/user/login",
		],
		'errorHandler' => [
				'errorAction' => 'site/error',
		],
		'log' => [
				'traceLevel' => YII_DEBUG? 3 : 0,
				'flushInterval' => 1,
				'targets' => [
						[
							'class' =>'yii\log\FileTarget',
							'levels' => ['error', 'warning'],
							'logFile' => '/data/logs/web/' . date('Ymd000', time()) . '_error.log' ,
							'maxFileSize' => 1024 * 16,
							'maxLogFiles' => 20,
							'exportInterval' => 1,
							'logVars' => [''],// 设置默认输出的日志，默认日志的上下文会包括$_GET, $_POST,$_FILES, $_COOKIE, $_SESSION, $_SERVER这些信息
						],
						[
							'class' => 'yii\log\FileTarget',
							'levels' => ['info'],
							'logFile' => '/data/logs/web/' . date('Ymd000', time()) . '_info.log' ,
							'maxFileSize' => 1024 * 16,
							'maxLogFiles' => 20,
							'exportInterval' => 1,
							'logVars' => [''],
						],
						[
							'class' => 'yii\log\FileTarget',
							'levels' => ['trace'],
							'logFile' => '/data/logs/web/' . date('Ymd000', time()) . '_trace.log' ,
							'maxFileSize' => 1024 * 16,
							'maxLogFiles' => 50,
							'exportInterval' => 1,
							'logVars' => [''],
						],
				],
		],
	),
	'params' => array(
		'captcha' => [
			'class' => 'yii\captcha\CaptchaAction',
		],
		'userTableName' => "{{%user}}",
		'supportEmail' => 'it@baomap.com',
		'emailAction' => 'user/resetpwd',
	)
);