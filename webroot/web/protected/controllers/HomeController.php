<?php

/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/9/19
 * Time: 17:15
 */
namespace app\controllers;
class HomeController extends \yii\web\Controller
{
	public function actionIndex()
	{

		return $this->render("index");
	}
}