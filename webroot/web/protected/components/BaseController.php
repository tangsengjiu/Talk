<?php

namespace app\components;

use common\lib\LController;
use common\misc\LError;
/**
 * Created by PhpStorm.
 * User: liujing
 * Date: 2016/3/6
 * Time: 19:58
 * @property string $jsMain
 * @property string $cssFile
 */
abstract class BaseController extends LController
{
    protected $_jsMain = null;
    protected $_cssFile = "global";
    protected $_needJs = false;
    protected $_pageTitle = "";

    public function getCssFile()
    {
        return $this->_cssFile;
    }

    public function getPageTitle()
    {
        return $this->_pageTitle;
    }

    public function setPageTitle($pageTitle)
    {
        $this->_pageTitle = $pageTitle;
    }

    public function setCssFile($name)
    {
        if ($name)
        {
            $this->_cssFile = "$name";
        }
    }

    public function getJsMain()
    {
        return $this->_jsMain;
    }

    public function setJsMain($name)
    {
        if ($name)
        {
            $this->_jsMain = "main/$name";
            $this->_needJs = true;
        }
    }


	public function filters()
	{
		return array(
			'accessControl',
		);
	}

    public function getJsDir()
    {
        return YII_DEBUG ? '/assets/src/js' : JS_PATH;
    }

	public function getAssetsDir()
	{
		return YII_DEBUG ? '/assets/src' : STATIC_PATH;
	}

    public function isAjaxRequest()
    {
        return Yii::$app->request->getIsAjax();
    }

    public function isPostRequest()
    {
        return Yii::$app->request->getIsPost();
    }

	public function getRequireConfig()
	{
		$urlArgs = 'v=' . STATIC_VER;
		$baseUrl = $this->getJsDir();
		return <<<config
var require = {
	urlArgs: '$urlArgs',
	baseUrl: '$baseUrl',
	waitSeconds: 5
};
config;
	}
}