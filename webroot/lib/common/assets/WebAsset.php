<?php
/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/7/20
 * Time: 19:19
 */

namespace common\assets;

use yii\web\AssetBundle;

class WebAsset extends AssetBundle
{
	public $basePath = BASE_PATH;
	public $baseUrl = BASE_URL;
	public $css = [];
	public $js = [];

	public $depends = [
		'yii\web\AssetBundle',
	];

	public $jsOptions = [
		'position' => \yii\web\View::POS_HEAD
	];

	public static function getCssPath()
	{
		return ASSETS_DIR . 'css/';
	}

    public static function getCdnCssPath()
    {
        return IMAGE_DIR .'web/css/';
    }

	public static function getImgPath()
	{
		return ASSETS_DIR . 'img/';
	}

    public static function getCdnImgPath()
    {
        return IMAGE_DIR .'web/';
    }

	public static function getJsPath()
	{
		return ASSETS_DIR . 'js/';
	}

    public static function getCdnJsPath()
    {
        return IMAGE_DIR .'web/js/';
    }

	//定义按需加载JS方法，注意加载顺序在最后
	public static function addScript($view, $jsfile) {
		$view->registerJsFile($jsfile, [AppAsset::className(), 'depends' => 'common\assets\WebAsset']);
	}

	//定义按需加载css方法，注意加载顺序在最后
	public static function addCss($view, $cssfile) {
		$view->registerCssFile($cssfile, [AppAsset::className(), 'depends' => 'common\assets\WebAsset']);
	}
}