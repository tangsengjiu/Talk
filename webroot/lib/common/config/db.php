<?php
/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/8/5
 * Time: 15:51
 */

return array(
	'components' =>array(
		'productDb' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=120.27.155.55;dbname=product', // MySQL, MariaDB

			'username' => 'baomap', //数据库用户名
			'password' => 'baomap', //数据库密码
			'charset' => 'utf8',
		],
		'userDb' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=120.27.155.55;dbname=user', // MySQL, MariaDB
			'username' => 'baomap', //数据库用户名
			'password' => 'baomap', //数据库密码
			'charset' => 'utf8',
		],
		'commonDb' => [
				'class' => 'yii\db\Connection',
				'dsn' => 'mysql:host=120.27.155.55;dbname=common', // MySQL, MariaDB
				'username' => 'baomap', //数据库用户名
				'password' => 'baomap', //数据库密码
				'charset' => 'utf8',
		],
		'guosenDb' => [
				'class' => 'yii\db\Connection',
				'dsn' => 'mysql:host=120.27.155.55;dbname=guosen', // MySQL, MariaDB
				'username' => 'baomap', //数据库用户名
				'password' => 'baomap', //数据库密码
				'charset' => 'utf8',
		],
		'orderDb' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=120.27.155.55;dbname=order', // MySQL, MariaDB
			'username' => 'baomap', //数据库用户名
			'password' => 'baomap', //数据库密码
			'charset' => 'utf8',
		],
		'actsDb' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=120.27.155.55;dbname=acts', // MySQL, MariaDB
			'username' => 'baomap', //数据库用户名
			'password' => 'baomap', //数据库密码
			'charset' => 'utf8',
		],
		'cache' => [
				'class' => 'yii\redis\Cache',
		],
		'redis' => [
				'class' => 'yii\redis\Connection',
				'hostname' => 'localhost',
				'port' => 6379,
				'database' => 0,
		],
	)
);