<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/21
 * Time: 15:49
 */
$baseConfig = require_once('base.php');
$dbConfig = ENVIRONMENT_DEV ? require_once('db_dev.php') : require_once('db.php');
$commonConfig = array(
	'language'=>'zh-CN',
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'bootstrap' => ['log'],
	'components' => array(
		'session' => [
			'class' => 'yii\web\Session',
//			'timeout' => 3600,

		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'viewPath' => '@common/mail',
			'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
//				'host' => 'smtp.qq.com',
//			    'username' => '1002946066@qq.com',
//				'password' => 'vvmrkwtmzkopbeca',
				'host' => 'smtp.exmail.qq.com',
				'username' => 'it@baomap.com',
				'password' => 'Baomap1it@',
				'port' => '465',
				'encryption' => 'ssl',
			],
		],
		'authManager' => [
				'class' => 'yii\rbac\PhpManager',
		],
		'assetManager' => [
				'converter' => [
						'class' => 'yii\web\AssetConverter',
						'commands' => [
								'less' => ['css', 'lessc {from} {to} --no-color'],
								'ts' => ['js', 'tsc --out {to} {from}'],
						],
				],
		],
		'urlManager' => [
			'class' => 'yii\web\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName'=>false,
			'rules'=>array(
			)
		],
	),
	'params' => array(
			'user.passwordResetTokenExpire' => 3600,
	),
);
return \yii\helpers\ArrayHelper::merge($baseConfig, $commonConfig, $dbConfig);