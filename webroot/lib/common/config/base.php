<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/21
 * Time: 15:49
 */
defined('FLAG_ORG_ATTR') or define('FLAG_ORG_ATTR', 0);
//环境
defined('ENVIRONMENT_DEV') or define('ENVIRONMENT_DEV', true); // true为开发环境，false为线上环境
defined('ENVIRONMENT') or define('ENVIRONMENT', ENVIRONMENT_DEV);
defined('ASSETS_DIR') or define('ASSETS_DIR', ENVIRONMENT_DEV ? "/assets/src/" : "/assets/dist/");
defined('BASE_PATH') or define('BASE_PATH', ENVIRONMENT_DEV ? "@source/src/" : "@source/dist/");
defined('BASE_URL') or define('BASE_URL', ENVIRONMENT_DEV ? "@source/src/" : "@source/dist/");
defined('BOWER_DIR') or define('BOWER_DIR', "@source/bower_components/");
defined('DEPS_DIR') or define('DEPS_DIR', "@source/deps/");
defined('IMAGE_DIR') or define('IMAGE_DIR', "http://image.baomap.com/"); //cdn图片


defined('ADMINROOT') or define('ADMINROOT', 'http://10.21.1.21:89/');
//defined('ADMINROOT') or define('ADMINROOT', 'http://localhost:91/');
defined('UPLOADS') or define('UPLOADS', dirname(dirname(dirname(__DIR__))) . "/uploads");

Yii::setAlias('@models', dirname(__DIR__) . '/models');
Yii::setAlias('@service', dirname(__DIR__) . '/service');
Yii::$classMap['LController'] = dirname(__DIR__) . '/lib/LController.php';
return array(
	'aliases' => array(
		'common' => dirname(__DIR__),
		'extensions' => dirname(dirname(__DIR__)) . '/extensions',
		'PhpAmqpLib' => dirname(__DIR__) . '/lib/rabbitMq/PhpAmqpLib',
	),
);