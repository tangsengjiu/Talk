<?php

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => array(
        'console' => dirname(__DIR__),
    ),
    'components' => [
		'log' => [
			'traceLevel' => YII_DEBUG? 3 : 0,
			'flushInterval' => 1,
			'targets' => [
				[
					'class' =>'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
					'logFile' => '/data/logs/cli/' . date('Ymd000', time()) . '_error.log' ,
					'maxFileSize' => 1024 * 16,
					'maxLogFiles' => 20,
					'exportInterval' => 1,
					'logVars' => [''],// 设置默认输出的日志，默认日志的上下文会包括$_GET, $_POST,$_FILES, $_COOKIE, $_SESSION, $_SERVER这些信息
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['info'],
					'logFile' => '/data/logs/cli/' . date('Ymd000', time()) . '_info.log' ,
					'maxFileSize' => 1024 * 16,
					'maxLogFiles' => 20,
					'exportInterval' => 1,
					'logVars' => [''],
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['trace'],
					'logFile' => '/data/logs/cli/' . date('Ymd000', time()) . '_trace.log' ,
					'maxFileSize' => 1024 * 16,
					'maxLogFiles' => 50,
					'exportInterval' => 1,
					'logVars' => [''],
				],
			],
		],
    ],
    'params' => array(
			'userTableName' => "{{%user}}",
			),
];