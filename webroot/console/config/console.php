<?php
/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/8/23
 * Time: 11:35
 */
$commonConfig = require_once(__DIR__ . '/../../lib/common/config/main.php');
$baseConfig = require_once('main.php');
defined('YII_DEBUG') or define('YII_DEBUG', true);
$config = \yii\helpers\ArrayHelper::merge($commonConfig, $baseConfig);
return $config;
