<?php
/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/8/29
 * Time: 17:46
 */

namespace console\controllers;


use common\models\GsFundshare;
use common\models\GsValuation;
use common\models\User;
use common\service\LCommonService;
use common\service\LGuoSenService;
use common\service\LUserService;
use yii\base\Exception;
use yii\console\Controller;

class GuoSenController extends Controller
{
	// 投资者持有份额查询
	public function actionFundshare()
	{
		echo "FundShare start:  \n";
		$appId = 'xbtlhj';
		$managerCode = '6898';
		$sign = strtoupper(md5('slmksaaowk8sd2oks877w'.'appId'.$appId.'managerCode'.$managerCode));
		$url =  'https://zctg.guosen.com.cn/xxl/fundshare/queryList?managerCode='.$managerCode.
				'&appId='.$appId.
				'&sign='.$sign;
		$data = json_decode(LCommonService::get($url));
		if(isset($data->code) && $data->code == 200)
		{
			$returnObject = isset($data->returnObject) ? $data->returnObject : null;
			if($returnObject && count($returnObject) > 0)
			{
				foreach($returnObject as $buyer)
				{
					// 交易号存在则跳过
					if(LGuoSenService::checkShareExist($buyer->tradeAccount))continue;
					try
					{
						$user = LUserService::getUserByIdCardNo(User::encryptAttribute('idCardNo', $buyer->idNumber));
						// 用户不存在则根据订单创建用户并且发送短信告知
						if(empty($user) && $buyer->idNumber)
						{
							$userCreated = LUserService::addOrderUser($buyer->idNumber);
							// 创建用户成功，则发送用户名和账号密码
							if($userCreated)
							{
								LUserService::sendNewUserEmail($userCreated->uid, "liujing@baomap.com");
								// TODO 木有手机号呢
							}
						}
						$uid = empty($user) ? 0 : $user->uid;
						if(LGuoSenService::addFundShare($uid, $buyer))
						{
							\Yii::trace(__CLASS__ . "_" . __FUNCTION__ . "msg[save fundShare success]  uid[{$uid}] tradeAccount[{$buyer->tradeAccount}]");
						}
						else
						{
							\Yii::error(__CLASS__ . "_" . __FUNCTION__ . "msg[save fundShare fail]  uid[{$uid}] tradeAccount[{$buyer->tradeAccount}]");
						}
					}
					catch(Exception $e)
					{
						\Yii::error(__CLASS__ . "_" . __FUNCTION__ . "msg[save fundShare exception]  message[{$e->getMessage()}]");
					}

				}
			}
			else
			{
				\Yii::trace(__CLASS__ . "_" . __FUNCTION__ . "msg[no new  fundShare data]");
			}
		}
	}
	public function actionCollectList()
	{
		for($i = "2016-07-01";$i < date('Y-m-d',time()); $i = date('Y-m-d',strtotime("+ 5 day", strtotime($i)))) {
			$appId = 'xbtlhj';
			$endTime = date("Y-m-d 00:00:00", strtotime($i));
			$managerCode = '6898';
			$startTime =  date('Y-m-d',strtotime("+ 5 day", strtotime($i)));
			$sign = strtoupper(md5('slmksaaowk8sd2oks877w' . 'appId' . $appId . 'endTime' . $endTime . 'managerCode' . $managerCode . 'startTime' . $startTime));
			$url = 'https://zctg.guosen.com.cn/xxl/entrust/queryList?managerCode=' . $managerCode .
					'&endTime=' . $endTime .
					'&appId=' . $appId .
					'&startTime=' . $startTime .
					'&sign=' . $sign;
			var_dump($url);
			$data = LCommonService::get($url);
			var_dump($data);
			echo "one over ~~~\n";
		}
	}

	public function actionEntrust()
	{
		for($i = "2016-08-31";$i < date('Y-m-d',time()); $i = date('Y-m-d',strtotime("+ 5 day", strtotime($i))))
		{
			$appId = 'xbtlhj';
			$endTime = date("Y-m-d 00:00:00", strtotime($i));
			$managerCode = '6898';
			$startTime =  date('Y-m-d',strtotime("+ 5 day", strtotime($i)));
			$sign = strtoupper(md5('slmksaaowk8sd2oks877w' . 'appId' . $appId . 'endTime' . $endTime . 'managerCode' . $managerCode . 'startTime' . $startTime));
			$url = 'https://zctg.guosen.com.cn/xxl/entrust/queryList?managerCode=' . $managerCode .
					'&endTime=' . $endTime .
					'&appId=' . $appId .
					'&startTime=' . $startTime .
					'&sign=' . $sign;
			var_dump($url);
			$data = LCommonService::get($url);
			var_dump($data);
			echo "one over ~~~\n";
		}
	}

	public function actionValuation()
	{
		echo "Valuation start:  \n";
		for($i = date('Y-m-d', strtotime("-5 days",time()));$i < date('Y-m-d',time()); $i = date('Y-m-d',strtotime("+ 1 day", strtotime($i))))
		{
			// 净值存在跳过
			if(LGuoSenService::checkValuationExist($i, "累计单位净值:"))continue;
			// 周六周天跳过
			if(date('w', strtotime($i)) == 0 || date('w', strtotime($i)) == 6)continue;
			echo $i  . "\n";
			$this->getValuationByDate($i);
			sleep(610);
		}
	}

	private function getValuationByDate($date)
	{
		try
		{
			$fundCode = "SL3712";
			$appId = 'xbtlhj';
			$managerCode = '6898';
			$sfqr = '1';
			$ywrq = $date;
			$sign = strtoupper(md5('slmksaaowk8sd2oks877w' . 'appId' . $appId . 'managerCode' . $managerCode . 'sfqr' . $sfqr . 'ywrq' . $ywrq));
			$url =  'https://zctg.guosen.com.cn/xxl/valuation/queryList?managerCode=' . $managerCode .
					'&ywrq=' . $ywrq .
					'&appId=' . $appId .
					'&sfqr=' . $sfqr .
					'&sign=' . $sign;
			$data = json_decode(LCommonService::get($url));
			if($data->code == 200)
			{
				$returnObject = isset($data->returnObject) ? $data->returnObject : null;
				if($returnObject && count($returnObject) > 0)
				{
					foreach($returnObject as $valuation)
					{
						var_dump($valuation);
						if(isset($valuation->kmdm) && ($valuation->kmdm == "基金单位净值:" || $valuation->kmdm == "累计单位净值:"))
						{
							if(LGuoSenService::addValuation($valuation, $fundCode))
							{
								\Yii::trace(__CLASS__ . "_" . __FUNCTION__ . " msg[save valuation success]  date[{$valuation->ywrq}] fundCode[{$fundCode}]");
							}
							else
							{
								\Yii::trace(__CLASS__ . "_" . __FUNCTION__ . " msg[save valuation fail]  date[{$valuation->ywrq}] fundCode[{$fundCode}]");
							}
						}
					}
				}
			}
			else
			{
				\Yii::error(__CLASS__ . "_" . __FUNCTION__ . "msg[access valuation api fail]  code[{$data->code}]");
			}

		}
		catch(Exception $e)
		{
			\Yii::error(__CLASS__ . "_" . __FUNCTION__ . "msg[access valuation api exception]  message[{$e->getMessage()}]");
		}

	}
}