<?php

namespace console\controllers;
/**
 * Created by PhpStorm.
 * User: wukong
 * Date: 2016/8/23
 * Time: 11:40
 */
use yii\base\Exception;
use yii\console\Controller;
use common\lib\LAMPQTransport;
class TestController extends Controller
{
	/**
	 * 	php yii.php test/index
	 * This command echoes what you have entered as the message.
	 * @param string $message the message to be echoed.
	 */
	public function actionIndex($message = 'hello world')
	{
		echo $message . "\n";
	}

	// php yii.php test/create
	public function actionCreate()
	{
		echo 'advanced';
	}

	/**
	 * cd console ->  php yii.php test/test-param test2(param) hello(param)
	 * @param $str
	 * @param $str2
	 */
	public function actionTestParam($str, $str2)
	{
		echo $str . "___" . $str2;
	}

	public function actionConsumer()
	{
		LAMPQTransport::consumer('wukong', 'msgs', "routingkey.test", 'consumer', __CLASS__ . '::process_message');
	}

	public function actionWorker()
	{
		LAMPQTransport::worker('wukong', 'msgs', 'testing send msg_body');
	}

	public static function process_message($msg) { //	public static

		echo "receive message : " . $msg->body . "\n";

		$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

	}

	// redis 实现消息队列----生产者
	public function actionTestRedisWorker()
	{
		echo "worker begin: \n";
		for($i =1 ;$i < 100; $i++)
		{
			echo $i . "~ \n";
			LAMPQTransport::redisWorkerQueue("testQueue", $i);
		}
	}

	// redis 实现消息队列----消费者
	public function actionTestRedisConsumer()
	{
		echo "worker begin: \n";
		while (true)
		{
			$val = null;
			try
			{
				$val = LAMPQTransport::redisConsumerQueue("testQueue");
				if ($val)
				{
					// TODO
					echo $val. "\n";
				}
				else
				{
					sleep(10);
				}
			}
			catch(Exception $e)
			{
				// 处理失败将数据丢回队列中
				if ($val)
				{
					$arr = \Yii::$app->cache->get("testQueue");
					$arr[] = $val;
					\Yii::$app->cache->set("testQueue", $arr);
				}
				\Yii::error(__CLASS__ . "_" . __FUNCTION__ . "msg[{$e->getMessage()}] queueName[testQueue] val[{$val}]");
			}
		}
	}

	public function actionRedisIncr()
	{
		// 测试redis incr和decr 可用户发短信次数限制
		\Yii::$app->getCache()->redis->set("test", 1);
		for($i = 0;$i < 10; $i++ )
		{
			\Yii::$app->getCache()->redis->incr("test");
		}
		$e = \Yii::$app->getCache()->redis->get("test");
		echo $e . "~~";


		for($i = 0;$i < 5; $i++ )
		{
			\Yii::$app->getCache()->redis->decr("test");
		}
		$e = \Yii::$app->getCache()->redis->get("test");
		echo $e . "~~";
	}
}